<?php

namespace Liuhuashi\DBListen;

use Dcat\Admin\Extend\ServiceProvider;
use Illuminate\Database\Events\QueryExecuted;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class DbListenServiceProvider extends ServiceProvider
{
    public function init()
    {
        parent::init();

        if (env('DB_LISTEN', true)) {
            DB::listen(function (QueryExecuted $query) {
                $this->dbFlag = 0;
                $result = preg_replace_callback('/\?/', function ($matches) use ($query) {
                    $bindings = $query->bindings[$this->dbFlag++];
                    if (!is_string($bindings)) {
                        $bindings = json_encode($bindings);
                    }
                    return $query->connection->getPdo()->quote($bindings);
                }, $query->sql);
                Storage::build(['driver' => 'local', 'root' => storage_path('logs')])->append('db-listen.md', $result . ';');
            });

            $result = sprintf("```\n#### 请求行 %s /%s", request()->getMethod(), request()->path());
            $result .= sprintf("\n> 请求头\n```php\n");
            $headers = request()->headers->all();
            foreach ($headers as $key => $value) {
                if (!in_array($key, ['accept-language', 'accept-encoding', 'sec-fetch-dest', 'sec-fetch-user', 'sec-fetch-mode', 'sec-fetch-site', 'accept',
                    'user-agent', 'upgrade-insecure-requests', 'sec-ch-ua-platform', 'sec-ch-ua-mobile', 'sec-ch-ua', 'cache-control',
                    'connection', 'content-length', 'content-type', 'host', 'x-csrf-token', 'x-pjax', 'x-pjax-container'])) {
                    $result .= sprintf("%-20s%s\n", $key . ':', implode('<br>', $value));
                }
            }
            $result .= sprintf("```\n");
            if (!in_array(request()->getMethod(), ['GET'])) {
                $result .= sprintf("> 请求体\n```php\n");
                foreach (request()->all() as $key => $item) {
                    $result .= sprintf("%-20s%s\n", $key . ':', $item);
                }
                $result .= "```\n> SQL语句\n```php";
            } else {
                $result .= "\n> SQL语句\n```php";
            }

            Storage::build(['driver' => 'local', 'root' => storage_path('logs')])->append('db-listen.md', $result);
        }
    }
}
